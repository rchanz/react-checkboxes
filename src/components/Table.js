import React from "react";

function Table({ people, editRecord }) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Selected Courses</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {people.map((person) => (
          <tr key={person.id}>
            <td>{person.id}</td>
            <td>{person.name}</td>
            <td>
              <ul>
                {person.selectedSubjects.map((subject) => (
                  <li key={subject}>{subject}</li>
                ))}
              </ul>
            </td>
            <td>
              <button className="btn btn-primary">Show</button>
              <button
                className="btn btn-warning"
                onClick={() => editRecord(person.id)}
              >
                Edit
              </button>
              <button className="btn btn-danger">Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default Table;
