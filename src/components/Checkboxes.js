import React from "react";

function Checkboxes({ forRenders, handleCheck }) {
  return (
    <div>
      {forRenders.map((subject) => (
        <label key={subject.id}>
          <input
            key={subject.id}
            type="checkbox"
            value={subject.value}
            checked={subject.isChecked}
            onChange={(e) => handleCheck(e, subject.id)}
          />
          {subject.value}&nbsp;
        </label>
      ))}
    </div>
  );
}

export default Checkboxes;
