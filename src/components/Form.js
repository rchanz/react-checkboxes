import React from "react";
import Checkboxes from "./Checkboxes";

function Form({
  subjects,
  initName,
  handleSubmit,
  handleChange,
  handleCheck,
  isEditing,
}) {
  return (
    <form className="form-group" onSubmit={handleSubmit}>
      <div className="inputName">
        <label htmlFor="name">Name: </label>
        <input
          type="text"
          name="name"
          value={initName}
          onChange={handleChange}
        />
      </div>
      <Checkboxes
        forRenders={subjects}
        handleSubmit={handleSubmit}
        handleCheck={handleCheck}
      />
      <button className={isEditing ? "btn btn-success" : "btn btn-primary"}>
        {isEditing ? "Update" : "Add"}
      </button>
    </form>
  );
}

export default Form;
