import React, { Component } from "react";
import Form from "./components/Form";
import Table from "./components/Table";
import "./App.css";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      name: "",
      subjects: [
        { id: 1, value: "Physics", isChecked: false },
        { id: 2, value: "Math", isChecked: false },
        { id: 3, value: "JavaScript", isChecked: false },
        { id: 4, value: "Atom Science", isChecked: false },
        { id: 5, value: "Literature", isChecked: false },
      ],
      selectedSubjects: [],
      people: [],
      editItem: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let people = this.state.people;
    const id = this.state.id;
    const name = this.state.name;
    const selectedSubject = this.state.selectedSubjects;
    const person = {
      id: Math.floor(Math.random() * 100),
      name: name,
      selectedSubjects: selectedSubject,
    };

    // people.push(person);

    const updateItems = [...this.state.people, person];

    this.setState({
      people: updateItems,
      selectedSubjects: [],
      name: "",
      subjects: [
        { id: 1, value: "Physics", isChecked: false },
        { id: 2, value: "Math", isChecked: false },
        { id: 3, value: "JavaScript", isChecked: false },
        { id: 4, value: "Atom Science", isChecked: false },
        { id: 5, value: "Literature", isChecked: false },
      ],
      editItem: false,
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleCheck = (e, id) => {
    let subjects = this.state.subjects;
    let checked = e.target.checked;

    //bind "checked" attribute with object's field "isChecked"
    subjects.map((data) => {
      if (data.id === id) {
        data.isChecked = checked;
      }
      return checked;
    });

    // subjects.map((data) => {
    //   if (data.isChecked) {
    //     t[data.id] = data.value;
    //   } else {
    //     t.splice(data.id, 1);
    //   }
    // });

    // take only checked objects (above commented code kinda explain the process)
    const final = subjects
      .filter((subject) => subject.isChecked !== false)
      .map((subject) => {
        // return { id: subject.id, value: subject.value };
        return subject.value;
      });

    console.log(final);

    this.setState({
      selectedSubjects: final,
    });
  };

  handleEdit = (id) => {
    // console.log(id);
    const filtered = this.state.people.filter((person) => person.id !== id);

    const selected = this.state.people.find((person) => person.id === id);

    const selectedSubjects = selected.selectedSubjects;

    const subjects = this.state.subjects;

    selectedSubjects.forEach((element) => {
      let foundIndex = subjects.findIndex((e) => e.value == element);
      subjects[foundIndex].isChecked = true;
    });
    // kdet.filter((k) =>
    //   k.value === "Math" ? (k.isChecked = true) : k.isChecked
    // );

    this.setState({
      people: filtered,
      name: selected.name,
      subjects: subjects,
      editItem: true,
      id: id,
    });
  };

  render() {
    return (
      <div className="App">
        <Form
          subjects={this.state.subjects}
          initName={this.state.name}
          handleSubmit={this.handleSubmit}
          handleChange={this.handleChange}
          handleCheck={this.handleCheck}
          isEditing={this.state.editItem}
        />
        <Table people={this.state.people} editRecord={this.handleEdit} />
      </div>
    );
  }
}

export default App;
